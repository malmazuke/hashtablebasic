public class HashTableBasic{
	private final static int TABLE_SIZE = 128;

	private HashEntry[] table;	

	public HashTableBasic(){
		table = new HashEntry[TABLE_SIZE];
		for (int i = 0; i < table.length; i++){
			table[i] = null;
		}
	}

	public void put(int key, int value){
		int hash = (key % TABLE_SIZE);

		while ((table[hash] != null) && (table[hash].getKey() != key))
			hash++;
		table[hash] = new HashEntry(key, value);
	}

	public int get(int key){
		int hash = (key % TABLE_SIZE);

		while (table[hash] != null && table[hash].getKey() != key)
			hash++;
		
		if (table[hash] == null)
			return -1;
		else
			return table[hash].getValue();
	}
	
	public void printTable(){
		for (int i = 0; i < table.length; i++){
			if (table[i] != null)
				System.out.println(i + ": " + table[i].getKey() + " - " + table[i].getValue());
		}
	}

	private class HashEntry{
		private int key;
		private int value;

		public HashEntry(int key, int value){
		this.key = key;
			this.value = value;
		}
		
		public int getKey(){
			return this.key;
		}
		
		public int getValue(){
			return this.value;
		}
	}
}
