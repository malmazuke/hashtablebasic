A very basic implementation of a Hash Table in Java.

Will create a table up to the maximum size (128 in this case).
If more elements are added beyond this, it will go into an infinite loop.

This bug could be solved by using Dynamic Arrays, and rehashing.
